# PROJEKT APLIKACJI MOBILNEJ W CELU ZALICZENIA PRZEDMIOTU "PAMO"

## Mobile Application: "RateMyCar"
	PJATK, Programowanie Aplikacji Mobilnych - PAMO
	Leader: Paweł Czapiewski

## Team:
	s13144, Mateusz Szczukowski
	s14329, Szymon Polak
	
## Idea:
	The application is written in Kotlin. 
	The application is used for ratings cars images where the ratings are saved in the database.
	Users are able to browse the cars ratings.
	