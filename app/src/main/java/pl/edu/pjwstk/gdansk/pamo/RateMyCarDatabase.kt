package pl.edu.pjwstk.gdansk.pamo

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

/**
 * RateMyCarDatabase abstract class
 *
 * @author Szymon Polak
 */

@Database(entities = arrayOf(Car::class), version = 1, exportSchema = false)
abstract class RateMyCarDatabase : RoomDatabase() {

    abstract fun carDao(): CarDao

    companion object {
        @Volatile
        private var INSTANCE: RateMyCarDatabase? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): RateMyCarDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance: RateMyCarDatabase = Room.databaseBuilder(
                    context.applicationContext,
                    RateMyCarDatabase::class.java,
                    "RateMyCar_database"
                )
                    .addCallback(RateMyCarDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                instance
            }
        }

        private class RateMyCarDatabaseCallback(
            private val scope: CoroutineScope
        ) : RoomDatabase.Callback() {

            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                INSTANCE?.let { database ->
                    scope.launch {
                        populateDatabase(database.carDao())
                    }
                }
            }
        }

        fun populateDatabase(carDao: CarDao) {

            carDao.deleteAllCars()

            var car = Car(
                0,
                "Honda Accord",
                "The Honda Accord is a series of automobiles manufactured by Honda since 1976, best known for its four-door sedan variant, which has been one of the best-selling cars in the United States since 1989. ~Wikipedia",
                "a",
                12
            )

            carDao.createCar(car)

            car = Car(
                1,
                "BMW 6 Series",
                "The BMW 6 Series is a range of grand tourers produced by BMW since 1976. It is the successor to the E9 Coupé and is currently in its fourth generation. ~Wikipedia",
                "b",
                9
            )

            carDao.createCar(car)

            car = Car(
                2,
                "Ford Fiesta",
                "The Ford Fiesta is a supermini marketed by Ford since 1976 over seven generations, including in United Kingdom, Germany, Valencia, Australia, Brazil, Argentina, Venezuela, Mexico, China, India, Thailand, and South Africa. It has been manufactured in many countries. ~Wikipedia",
                "c",
                8
            )

            carDao.createCar(car)

            car = Car(
                4,
                "Toyota Supra",
                "The Toyota Supra is a sports car and grand tourer manufactured by Toyota Motor Corporation beginning in 1978. The initial four generations of the Supra were produced from 1978 to 2002. ~Wikipedia",
                "d",
                10
            )

            carDao.createCar(car)

            car = Car(
                5,
                "Mercedes-Benz SL-Class",
                "The Mercedes-Benz SL-Class is a grand touring car manufactured by Mercedes since 1954. The designation SL derives from the German Super-Leicht, (English: Super Light). ~Wikipedia",
                "e",
                11
            )

            carDao.createCar(car)
        }
    }
}