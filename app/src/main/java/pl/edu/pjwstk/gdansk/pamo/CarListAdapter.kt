package pl.edu.pjwstk.gdansk.pamo

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_of_cars.view.*

/**
 * CarListAdapter class
 *
 * @author Szymon Polak
 */

class CarListAdapter internal constructor(context: Context) :
    RecyclerView.Adapter<CarListAdapter.CarViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var cars = emptyList<Car>()

    inner class CarViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val carItemViewName: TextView = itemView.findViewById(R.id.carNameView)
        val carItemViewRate: TextView = itemView.findViewById(R.id.carRateView)
        val carItemViewImage: ImageView = itemView.findViewById(R.id.carImageView)
        val carItemViewNumber: TextView = itemView.findViewById(R.id.numberOfCarView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarViewHolder {
        val itemView = inflater.inflate(R.layout.row_of_cars, parent, false)
        return CarViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: CarViewHolder, position: Int) {
        val current = cars[position]
        holder.carItemViewName.carNameView.text = current.title
        holder.carItemViewRate.carRateView.text = current.ratings.toString()
//        Parameter which is inside "setImageResource" method should be a value from DB like line below.
//        holder.carItemViewImage.carImageView.setImageResource(Resources.getSystem().getIdentifier(current.image, "drawable", packageName))
        holder.carItemViewImage.carImageView.setImageResource(R.drawable.a)
        holder.carItemViewNumber.numberOfCarView.text = (position + 1).toString()
    }

    internal fun setCars(cars: List<Car>) {
        this.cars = cars
        notifyDataSetChanged()
    }

    internal fun addRate(position: Int, rate: Int) {
        this.cars[position].ratings += rate
        notifyDataSetChanged()
    }

    override fun getItemCount() = cars.size
}