package pl.edu.pjwstk.gdansk.pamo

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * CarViewModel class
 *
 * @author Szymon Polak
 */

class CarViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: CarRepository
    val allCars: LiveData<List<Car>>

    init {
        val carDao = RateMyCarDatabase.getDatabase(application, viewModelScope).carDao()
        repository = CarRepository(carDao)
        allCars = repository.allCars
    }

    fun readCar(carId: Int) = viewModelScope.launch(Dispatchers.IO) {
        repository.readCar(carId)
    }

    fun createCar(car: Car) = viewModelScope.launch(Dispatchers.IO) {
        repository.createCar(car)
    }

    fun updateCarRatings(ratings: Int, carId: Int) = viewModelScope.launch(Dispatchers.IO) {
        repository.updateCarRatings(ratings, carId)
    }

    fun deleteCar(car: Car) = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteCar(car)
    }

    fun deleteAllCars() = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteAllCars()
    }
}