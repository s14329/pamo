package pl.edu.pjwstk.gdansk.pamo

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

/**
 * CarDao interface
 *
 * @author Szymon Polak
 */

@Dao
interface CarDao {

    @Insert
    fun createCar(car: Car)

    @Query("SELECT * FROM cars_table WHERE car_id = :carId")
    fun readCar(carId: Int): LiveData<Car>

    @Query("UPDATE cars_table SET ratings = :ratings WHERE car_id = :carId")
    fun updateCarRatings(ratings: Int, carId: Int)

    @Delete
    fun deleteCar(car: Car)

    //    @Query("SELECT * FROM cars_table ORDER BY ratings DESC")
//    fun getAllCars(): LiveData<List<Car>>
    @Query("SELECT * FROM cars_table ORDER BY ratings DESC")
    fun getAllCars(): LiveData<List<Car>>

    @Query("DELETE FROM cars_table")
    fun deleteAllCars()
}