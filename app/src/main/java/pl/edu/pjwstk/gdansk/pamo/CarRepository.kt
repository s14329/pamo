package pl.edu.pjwstk.gdansk.pamo

import android.os.AsyncTask
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData

/**
 * CarRepository class
 *
 * @author Szymon Polak
 */

class CarRepository(private val carDao: CarDao) {

    val allCars: LiveData<List<Car>> = carDao.getAllCars()

    fun readCar(carId: Int): LiveData<Car> {
        return carDao.readCar(carId)
    }

    @WorkerThread
    fun createCar(car: Car) {
        insertAsyncTask(carDao).execute(car)
//        carDao.createCar(car)
    }

    @WorkerThread
    fun updateCarRatings(ratings: Int, carId: Int) {
        carDao.updateCarRatings(ratings, carId)
    }

    @WorkerThread
    fun deleteCar(car: Car) {
        carDao.deleteCar(car)
    }

    @WorkerThread
    fun deleteAllCars() {
        carDao.deleteAllCars()
    }

    private class insertAsyncTask internal constructor(private val mAsyncTaskDao: CarDao) :
        AsyncTask<Car, Void, Void>() {

        override fun doInBackground(vararg params: Car): Void? {
            mAsyncTaskDao.createCar(params[0])
            return null
        }
    }
}