package pl.edu.pjwstk.gdansk.pamo

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.RatingBar
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_rating.*


/**
 * RatingActivity class
 *
 * @author Szymon Polak
 */

class RatingActivity : AppCompatActivity(), RatingBar.OnRatingBarChangeListener {

    private lateinit var carViewModel: CarViewModel
    var cars: List<Car> = emptyList<Car>()

    private var carPosition = POSITION_NOT_SET

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rating)

        //
//        carViewModel = ViewModelProviders.of(this).get(CarViewModel::class.java)
//        carViewModel.allCars.observe(this, Observer { thisCars ->
//            thisCars?.let {cars = it}
//        })
//        var blah: CarDao
//        blah.getAllCars().observe(cars)
//        Log.d("SIEMANKO", cars.toString())
//        Log.d("SIEMANKO", cars.size.toString())
        //

        displayCar()

        ratingBar_rating.onRatingBarChangeListener = this

        button_home2.setOnClickListener { _ ->
            val activityIntent = Intent(this, MainActivity::class.java)
            startActivity(activityIntent)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun displayCar() {

//        val car = cars[carPosition]
        text_rating_car_name.text = cars[carPosition].title
        text_rating_car_description.text =
                ((cars[carPosition].description) + "\n\nCurrent rate: " + (cars[carPosition].ratings.toString()))
        imageView.setImageResource(resources.getIdentifier(cars[carPosition].image, "drawable", packageName))
        val bar: RatingBar = findViewById(R.id.ratingBar_rating)
        button_rating_submit.setOnClickListener { _ ->
            //                        car.ratings.(ratingBar_rating.numStars)
//            cars[carPosition].ratings += ratingBar_rating.numStars
            carViewModel.updateCarRatings(ratingBar_rating.numStars, carPosition)
            if (carPosition < cars.size - 1) {
                carPosition++
                displayCar()
            } else {
                finish()
            }
        }
        button_rating_submit.isEnabled = false
        bar.rating = 0F
        if (carPosition == cars.size - 1) {
            button_rating_submit.isEnabled = true
            button_rating_submit.text = getString(R.string.finish)
        }
    }

//    @SuppressLint("SetTextI18n")
//    private fun displayCar() {
//
//
//        val car = cars[carPosition]
//        text_rating_car_name.text = car?.title
//        text_rating_car_description.text =
//            ((car?.description) + "\n\nPrevious ratings: " + (car?.ratings.toString()))
//        imageView.setImageResource(resources.getIdentifier(car?.image, "drawable", packageName))
//        val bar: RatingBar = findViewById(R.id.ratingBar_rating)
//        button_rating_submit.setOnClickListener { _ ->
//            //            car?.ratings?.add(ratingBar_rating.numStars)
////            if (carPosition < cars.size - 1) {
////                carPosition++
////                displayCar()
////            } else {
////                finish()
////            }
//        }
//        button_rating_submit.isEnabled = false
//        bar.rating = 0F
//        if (carPosition == cars.size - 1) {
//            button_rating_submit.isEnabled = true
//            button_rating_submit.text = getString(R.string.finish)
//        }
//    }

    override fun onRatingChanged(p0: RatingBar?, p1: Float, p2: Boolean) {
        button_rating_submit.isEnabled = (p1.toInt() != 0)
    }
}