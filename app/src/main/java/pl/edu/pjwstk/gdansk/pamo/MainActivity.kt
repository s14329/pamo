package pl.edu.pjwstk.gdansk.pamo

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.system.exitProcess

/**
 * MainActivity class
 *
 * This is main class of project.
 * @author Szymon Polak
 * @author Mateusz Szczukowski
 *
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//      Go to start rating view
        button_start.setOnClickListener { _ ->
            val activityIntent = Intent(this, RatingActivity::class.java)
            startActivity(activityIntent)
        }
//      Go to rank view
        button_rank.setOnClickListener { _ ->
            val activityIntent = Intent(this, ListOfCarsRatingsActivity::class.java)
            startActivity(activityIntent)
        }
//      GO to about view
        button_about.setOnClickListener { _ ->
            val activityIntent = Intent(this, AboutActivity::class.java)
            startActivity(activityIntent)
        }
//      Exit
        button_exit.setOnClickListener { _ ->
            moveTaskToBack(true);
            exitProcess(-1)
        }
    }
}