package pl.edu.pjwstk.gdansk.pamo

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_list_of_cars_ratings.*

/**
 * ListOfCarsRatingsActivity class
 *
 * @author Szymon Polak
 * @author Mateusz Szczukowski
 *
 */
class ListOfCarsRatingsActivity : AppCompatActivity() {

    private lateinit var carViewModel: CarViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_of_cars_ratings)

        val recyclerView = findViewById<RecyclerView>(R.id.listViewOfCarsRatings)
        val adapter = CarListAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        carViewModel = ViewModelProviders.of(this).get(CarViewModel::class.java)
        carViewModel.allCars.observe(this, Observer { cars ->
            cars?.let { adapter.setCars(it) }
        })

//        listView.adapter = MyAdapter(this)

        button_rating.setOnClickListener { _ ->
            val activityIntent = Intent(this, RatingActivity::class.java)
            startActivity(activityIntent)
        }

        button_home.setOnClickListener { _ ->
            val activityIntent = Intent(this, MainActivity::class.java)
            startActivity(activityIntent)
        }
    }

//    private class MyAdapter(context: Context): BaseAdapter() {
//
//        private val mContext: Context
//
//        private val cars = arrayListOf<String>(
//            "Car 1", "Car 2", "Car 3", "Car 4", "Car 5"
//        )
//
//        private val rates = arrayListOf<String>(
//            "4.5", "3", "5", "4.2", "3.8"
//        )
//
//        init {
//            mContext = context
//        }
//
//        override fun getCount(): Int {
//            return cars.size
//        }
//
//        override fun getItemId(position: Int): Long {
//            return position.toLong()
//        }
//
//        override fun getItem(position: Int): Any {
//            return ""
//        }
//
//        override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
////            val textView = TextView(mContext)
////            textView.text = "Car name: \nRate: "
////            return textView
//
//            val layoutInflater = LayoutInflater.from(mContext)
//            val rowOfCar = layoutInflater.inflate(R.layout.row_of_cars, viewGroup, false)
//
//            val numberOfCar = rowOfCar.findViewById<TextView>(R.id.numberOfCarView)
//            numberOfCar.text = (position + 1).toString()
//
////            val carImage = rowOfCar.findViewById<TextView>(R.id.carImageView)
//
//            val carName = rowOfCar.findViewById<TextView>(R.id.carNameView)
//            carName.text = cars.get(position)
//
//            val carRate = rowOfCar.findViewById<TextView>(R.id.carRateView)
//            carRate.text = rates.get(position)
//
//            return rowOfCar
//        }
//    }
}
