package pl.edu.pjwstk.gdansk.pamo

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Car data class
 *
 * @author Szymon Polak
 */

@Entity(tableName = "cars_table")
data class Car(
        @PrimaryKey @ColumnInfo(name = "car_id") var carId: Int,
        var title: String,
        var description: String,
        var image: String,
        var ratings: Int
) {
    override fun toString(): String {
        return "Car(carId=$carId, title='$title', description='$description', image='$image', ratings=$ratings)"
    }
}