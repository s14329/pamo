package pl.edu.pjwstk.gdansk.pamo

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.io.IOException

//@RunWith(AndroidJUnit4::class)
class SampleEntityTest {
    private lateinit var carDao: CarDao
    private lateinit var db: RateMyCarDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
                context, RateMyCarDatabase::class.java).build()
        carDao = db.carDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    fun writeCarAndRead() {
        val car = Car(10, "Alfa Romeo", "Example description", "x", 10)
        carDao.createCar(car)
        val car: LiveData<Car> = liveData {
            emit
        }
        Log.d("HELLO", carById.toString())
        Log.d("HELLO", carById.value.toString())
        Assert.assertEquals(carById, car)
    }
}
